#Drop#

This is my first released game for one game every two weeks/one month project.
Very simple two player vs game, played on same keyboard.

You can download build jar from:

* [Dropbox](https://dl.dropboxusercontent.com/u/9278433/dagondev/drop/Drop-1.0.zip)
* [Mediafire](https://www.mediafire.com/?v3vac2qfq0f9mvo)


For some inside view about creating this ~~beautiful piece of work~~ clusterfuck, and some info about why the fuck all of the game is located in two big classes, look at [my post about it](link)

###"Plot"###

You are the red rectangle, and your enemy is the blue one.
Both of you are in some arena populated by rectangles and ellipses.
You must fight to death of another.


The World is a cruel place.

###Rules###

To achieve victory you can:

* Push enemy off the arena
* Drop an object on top of the enemy
* Hit the ring on top of the arena

###Code###

Before you start looking at this ~~anus of Satan~~ code, remember that it is mostly example how **not** to write a game, even simple one.

######Logic is divided between `Screen` implementations which contains all logic for specific work. (Main menu/Game)######

#####DesktopLauncher#####

Starting class. Creates object of `DropGame` and passes it to `LwjglApplication` which handles magic of creating window, creating loop etc. Meh.

#####DropGame#####

Main class for game itself, contains all objects that can be shared between Screens, handles creation of starting Screen - `MainMenuScreen`.

*If your game is **really** simple, you could pack all logic to this class instead making Screens but you never should make `one class to rule them all`, so don`t.*

#####MainMenuScreen#####

Responsible for drawing stuff at start of the game and creating Screen - `GameScreen`.
Interesting stuff is happening in constructor and `render(float delta)` method.

#####GameScreen#####

This is where the magic happens. You should be interested in this methods:

* `create()` - setting all important data and creating stuff for game
* `render()` - main loop of game, containing well... rendering stuff and calls for:
* * `preRenderUpdate(float delta)` - (spoiler alert it is called before any rendering stuff happens) Updates camera.
* * `postRenderUpdate(float delta)` - (after all rendering or all logic to be specific) Handles switch to Main Menu. Important to change screens after all logic because of `dispose()` call.
* `handleInput()` - handles most of the input checking, by polling specific keys every tick
* `keyUp(int keycode)` - Event handler for input that handles release of keys and setting flag of resetting game/switching to mainmenu
* `beginContact(Contact contact)` - Event handler for physics that handles logic of rule `Hit the ring on top of the arena`
* `postSolve(Contact contact, ContactImpulse impulse)` - Event handler for physics that handles logic of rule `Drop an object on top of the enemy`
 
Those methods contains most important logic or calls other important methods. So with those you should go back and forth around all class and ~~gain some insanity points~~ knowledge about how game works.

Important data that can`t be changed in runtime was set in static final fields.

###Building###

* automatically via gradle
* * Because RubeLoader is not in maven repositories you need to add it manually! add RubeLoader.jar (if it is not there) to ${main.dir}/libs
* * Same deal with GifDecoder.
* * If you want use 1.3 version of Box2DLights (i got like 20 fps boost from it) of Box2DLights, you need to add it (if it is not there) to ${main.dir}/libs
* * * If you are going to use 1.2, you can comment `compile files('../libs/box2dlights-1.3-SNAPSHOT.jar')` and uncomment `compile "com.badlogicgames.box2dlights:box2dlights:1.2"` in ${main.dir}/build.gradle (project:core, dependencies)
* manually
* * starting class - `com.dagondev.drop.desktop.DesktopLauncher`
* * with resources located in core/assets - select this folder as resources in your project settings or just put files from this folder to main dir of jar/compiled classes
* * with dependencies stated below
* * * You can get most of the dependencies (libgdx, lwlgl, box2d, box2dlights) from Setup app, that will download them via gradle...

###Dependencies###

* [JDK](http://www.oracle.com/technetwork/java/javase/downloads/jdk7-downloads-1880260.html) 1.7.0_45 x86
* [LibGDX](http://libgdx.badlogicgames.com/download.html) 1.2.0
* * [JBox2D](http://libgdx.badlogicgames.com/download.html) 1.2.0 (embedded with gdx)
* * [Box2DLights](http://libgdx.badlogicgames.com/download.html)
* * * 1.3 - http://libgdx.badlogicgames.com/box2dlights/box2dlights-1.3-SNAPSHOT.jar or use jar from libs folder on repo
* * * 1.2 - via gradle/embedded with gdx
* * [LWJGL](http://lwjgl.org/) 2.9.1 (embedded with gdx)
* [RubeLoader](https://github.com/tescott/RubeLoader)
* * build with commit f66a28e030cf7e5e76b1cd4cb0ad5e19c1019af6
* * use jar from libs folder on repo
* [GifDecoder](http://www.badlogicgames.com/forum/viewtopic.php?f=11&t=10351&p=46673#p46673)
* * build yourself with code supplied on linked site
* * use jar from libs folder on repo

#####Used jars#####
* gdx-1.2.0.jar
* gdx-platform-1.2.0-natives-desktop.jar
* gdx-backend-lwjgl-1.2.0.jar
* gdx-box2d-1.2.0.jar
* gdx-box2d-platform-1.2.0-natives-desktop.jar
* RubeLoader.jar
* lwjgl-2.9.1.jar
* One of these:
* * lwjgl-platform-2.9.1-natives-windows.jar
* * lwjgl-platform-2.9.1-natives-linux.jar
* * lwjgl-platform-2.9.1-natives-osx.jar
* One of these:
* * box2dlights-1.3-SNAPSHOT.jar
* * box2dlights-1.2.jar
* GifDecoder.jar

###Copyright and License###

#####Drop#####

Drop is licensed under the Apache 2 License, meaning you can use it free of charge, without strings attached in commercial and non-commercial projects.

Copyright © 2014 Maciej 'dagon' Szewczyk

www.dagondev.com

#####For jars in libs folder#####

RubeLoader, GifDecoder libraries are under the Apache 2 License.