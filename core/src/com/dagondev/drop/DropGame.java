/*******************************************************************************
 * Copyright 2014 Maciej 'dagon' Szewczyk www.dagondev.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package com.dagondev.drop;

import com.badlogic.gdx.*;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;


public class DropGame extends Game {
    public SpriteBatch batch;
    public BitmapFont font;
    int baseWidth;
    int baseHeight;


    public DropGame(int baseWidth, int baseHeight) {
        this.baseWidth=baseWidth;
        this.baseHeight=baseHeight;
    }
    public void create() {
        batch = new SpriteBatch();
        //Use LibGDX's default Arial font.
        font = new BitmapFont();
        this.setScreen(new MainMenuScreen(this,baseWidth,baseHeight));
    }

    public void render() {
        super.render(); //important!
    }

    @Override
    public void resize(int width, int height) {
        //for not pixelated text when resize to fullscreen
       // batch = new SpriteBatch();
        super.resize(width, height);
    }

    public void dispose() {
        batch.dispose();
        font.dispose();
        super.dispose();
    }
}
